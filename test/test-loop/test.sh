#!/bin/bash
LOG_FILE="/init/init.log"

log() {
	echo "[Test] $*" >> "${LOG_FILE}"
}

test_postgres() {
	log "INFO Checking server status"
	if supervisorctl -u sysops -p "${SUPERVISOR_PASSWORD}" status postgres 2>&1 | grep -q "RUNNING" >> "${LOG_FILE}"  2>&1; then
		log "INFO Testing connection"
		if psql -w --username="${POSTGRES_USER}" "${POSTGRES_DB}" <<< '\d' >> "${LOG_FILE}" 2>&1; then
			log "INFO Successful DB connection"
			echo -e "HTTP/1.1 200 OK\n\n $(date)"
		else
			log "WARN Failed DB connection"
			echo -e "HTTP/1.1 500 Internal Server Error\n\n $(date)"
		fi
	else
		log "WARN Postgres not running"
		echo -e "HTTP/1.1 503 Service Unavailable\n\n $(date)"
	fi
}

main() {
	log "Incoming request $(date) $*"
	test_postgres
}

main "$@"
