#!/bin/sh

chown -R postgres:postgres /usr/share/postgresql/
chmod -R 0755 /usr/share/postgresql/

chown -R postgres:postgres /usr/lib/postgresql/
chmod -R 0755 /usr/lib/postgresql/

chown -R postgres:postgres /run/postgresql/
chmod -R 0775 /run/postgresql/

#Fix ownership and permission for postgres data directory
chown -R postgres:postgres "$PGDATA"
chmod 0700 "$PGDATA"

#
# Enable the specified configuration key ($1) and set
# its value to the specified value ($2) in the ppstgres
# configuration file.
#
replace_conf() {
    KEY=$1
    NEW_VALUE=$2

    if [ -z ${KEY+x} ]; then
        echo "Supplied no key"
    elif [ -z ${NEW_VALUE+x} ] || [ "${NEW_VALUE}" = "" ]; then
        echo "Supplied no value for key: ${KEY}"
    else
        echo "Setting ${KEY} to [${NEW_VALUE}]"

        #Fetch the line from the pg config file
        CUR_LINE=$(grep "${KEY} =" "${PGDATA}/postgresql.conf")
        #Count the number of # signs (can be 1 or 2 depending on if the line has a trailing comment or not)
        #Possible forms:
        #   key = value
        #   key = value #some comment
        #   #key = value
        #   #key = value #some comment
        #
        NUM_POUNDS=$(echo "${CUR_LINE}" | awk -F"#" '{print NF-1}')
        #CUT index has to add one
        CUT_IDX=$((NUM_POUNDS+1))
        #This is the current value of the key
        #CUR_VALUE="$(echo "${CUR_LINE}" | cut -d "=" -f 2 | cut -d "#" -f 1 | xargs)"
        #this is the comment section (can be empty)
        CUR_COMMENT="$(echo "${CUR_LINE}" | cut -d "#" -f ${CUT_IDX})"
        #Extract
        CUR_REPLACE="${CUR_LINE}"
        if [ "${CUR_COMMENT}" != "" ]; then
            CUR_REPLACE="$(echo "${CUR_LINE}" | sed "s/#${CUR_COMMENT}//g" | xargs)"
        fi

        SED_STRING="s/${CUR_REPLACE}/${KEY} = ${NEW_VALUE}/g"

        #Use sed to actually replace the value
        echo "sed command: '${SED_STRING}']"
        sed -i "${SED_STRING}" "$PGDATA"/postgresql.conf
    fi
}

#Check for PGDATA directory
if [ -z "${PGDATA}" ]; then
    echo "PGDATA environment variable is not set"
    exit 1
elif [ ! -e "${PGDATA}" ]; then
    echo "${PGDATA} does not exist"
    exit 1
elif [ ! -d "${PGDATA}" ]; then
    echo "${PGDATA} is not a directory"
    exit 1
fi

#Trigger initilization if the PGDATA directory is empty
if [  -z "$(ls -A "$PGDATA")" ]; then
    echo "================================="
    echo "= Initializing postges instance ="
    echo "================================="

    gosu postgres initdb

    echo "Updating configuration"
    sed -ri "s/^#(listen_addresses\s*=\s*)\S+/\1'*'/" "$PGDATA"/postgresql.conf
    replace_conf "shared_buffers" "${PG_SHARED_BUFFERS}"
    replace_conf "effective_cache_size" "${PG_EFFECTIVE_CACHE_SIZE}"
    replace_conf "checkpoint_completion_target" "${PG_CHECKPOINT_COMPLETION_TARGET}"
    replace_conf "maintenance_work_mem" "${PG_MAINTENANCE_WORK_MEM}"
    replace_conf "wal_buffers" "${PG_WAL_BUFFERS}"
    replace_conf "default_statistics_target" "${PG_DEFAULT_STATISTICS_TARGET}"
    replace_conf "random_page_cost" "${PG_RANDOM_PAGE_COST}"
    replace_conf "effective_io_concurrency" "${PG_EFFECTIVE_IO_CONCURRENCY}"
    replace_conf "work_mem" "${PG_WORK_MEM}"
    replace_conf "min_wal_size" "${PG_MIN_WAL_SIZE}"
    replace_conf "max_wal_size" "${PG_MAX_WAL_SIZE}"
    replace_conf "max_worker_processes" "${PG_MAX_WORKER_PROCESSES}"
    replace_conf "max_parallel_workers_per_gather" "${PG_PARALLEL_WORKERS_PER_GATHER}"


    : "${POSTGRES_USER:=postgres}"
    : "${POSTGRES_DB:=$POSTGRES_USER}"

    if [ "$POSTGRES_PASSWORD" ]; then
      pass="PASSWORD '$POSTGRES_PASSWORD'"
      authMethod=md5
    else
      echo "==============================="
      echo "!!! Use \$POSTGRES_PASSWORD env var to secure your database !!!"
      echo "==============================="
      pass=
      authMethod=trust
    fi
    echo


    if [ "$POSTGRES_DB" != 'postgres' ]; then
      createSql="CREATE DATABASE $POSTGRES_DB;"
      echo "$createSql" | gosu postgres postgres --single -jE
      echo
    fi

    if [ "$POSTGRES_USER" != 'postgres' ]; then
      op=CREATE
    else
      op=ALTER
    fi

    userSql="$op USER $POSTGRES_USER WITH SUPERUSER $pass;"
    echo "$userSql" | gosu postgres postgres --single -jE
    echo

    # internal start of server in order to allow set-up using psql-client
    # does not listen on TCP/IP and waits until start finishes
    gosu postgres pg_ctl -D "$PGDATA" \
        -o "-c listen_addresses=''" \
        -w start

    echo

	# shellcheck disable=SC1090
    for f in /docker-entrypoint-initdb.d/*; do
        case "$f" in
            *.sh)  echo "$0: running $f"; . "$f" ;;
            *.sql) echo "$0: running $f"; psql --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" < "$f" && echo ;;
            *)     echo "$0: ignoring $f" ;;
        esac
        echo
    done

    gosu postgres pg_ctl -D "$PGDATA" -m fast -w stop

    { echo; echo "host all all 0.0.0.0/0 $authMethod"; } >> "$PGDATA"/pg_hba.conf
fi