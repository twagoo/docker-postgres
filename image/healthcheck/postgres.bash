#!/bin/bash
POSTGRES_PORT="5432"

OPTS=('-t' '5' '-p' "${POSTGRES_PORT}")

if [ "${POSTGRES_USER}" ]; then
	OPTS+=('-U' "${POSTGRES_USER}")
fi

if [ "${POSTGRES_DB}" ]; then
	OPTS+=('-d' "${POSTGRES_DB}")
fi

gosu postgres pg_isready "${OPTS[@]}" > /dev/null || exit 1
